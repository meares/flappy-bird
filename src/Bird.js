export default class Bird {
    constructor(position) {
        if(typeof position === 'undefined') {
            position = {
                x: 64,
                y: 100
            };
        }

        this.position = position;
        this.gravity = 0.6;
        this.velocity = 0;
        this.lift = 15;
    }

    render(p5) {
        p5.fillStyle = 'rgb(255)';
        p5.ellipse(this.position.x, this.position.y, 32, 32);
    }

    update() {
        this.velocity += this.gravity;
        this.velocity *= 0.9;
        this.position.y += this.velocity;
    }

    moveUp() {
        this.velocity -= this.lift;
    }
}