export default class Pipe {
    constructor(margin, farRightSide) {
        if(typeof margin === 'undefined') {
            margin = {
                top: 50,
                bottom: 50
            }
        }

        if(typeof farRightSide === 'undefined') {
            farRightSide = 150;
        }

        this.margin = margin;
        this.x = farRightSide;
        this.w = 30;
        this.speed = 5;

        this.running = true;
    }

    render(p5) {
        this.canvasHeight = p5.canvas.height;

        p5.fill(255);

        console.log(this.running);
        if(!this.running) {
            p5.fill(255, 0, 0);
        }

        p5.rect(this.x, 0, this.w, this.margin.top);
        p5.rect(this.x, this.canvasHeight - this.margin.bottom, this.w, this.margin.bottom);
    }

    update() {
        if(this.running) {
            this.x -= this.speed;
        }
    }

    collides(object) {
        return object.position.x > this.x && object.position.x < this.x + this.w && (object.position.y < this.margin.top || object.position.y > this.canvasHeight - this.margin.bottom);
    }
}