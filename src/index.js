import p5 from 'p5';
import Bird from './Bird';
import Pipe from './Pipe';

const CANVAS_WIDTH = 400;
const CANVAS_HEIGHT = 600;

let createPipes = true;

const bird = new Bird({
    x: 64,
    y: CANVAS_HEIGHT / 2
});

const pipes = [];

const createNewPipe = _p5 => {
    pipes.push(new Pipe({
        top: _p5.random(CANVAS_HEIGHT / 2),
        bottom: _p5.random(CANVAS_HEIGHT / 2)
    }, CANVAS_WIDTH));
};

new p5(_p5 => {
    _p5.setup = () => {
        _p5.createCanvas(CANVAS_WIDTH, CANVAS_HEIGHT);
        createNewPipe(_p5);
    };

    _p5.draw = () => {
        _p5.background(0);
        bird.update();
        bird.render(_p5);

        if(createPipes && _p5.frameCount % 50 === 0) {
            createNewPipe(_p5);
        }

        for(let i = pipes.length - 1; i >= 0; i--) {
            pipes[i].update();
            pipes[i].render(_p5);

            if(pipes[i].collides(bird)) {
                createPipes = false;

                pipes.map(pipe => {
                    pipe.running = false;
                });

                bird.velocity = 0;
                bird.gravity = 0;
            }

            if(pipes[i].x < 0 - pipes[i].w) {
                pipes.splice(i, 1);
            }
        }
    };

    _p5.keyPressed = event => {
        if(event.key == ' ') {
            bird.moveUp();
        }
    }
}, document.getElementById('game-hook'));